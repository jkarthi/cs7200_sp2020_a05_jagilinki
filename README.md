# CS7200_SP2020_A05_Jagilinki
---------------------------------------------

**Summary:**

The program generically allows to define the number of inputs, number of hidden layers and outputs.
Hidden layers is defined as a list, where the length of the list represents the number of hidden layers. And the value of each element of the list indicates the number of neurons to be present for each of the hidden layer
Weights for the connections is randomly defined by using random number generator

To compute backpropagation, we need information about the activations and derivatives for computing the gradient descent.
Trained the network via backpropagation and performed gradient descent to adjust the weights for each epoch.

Once the training is complete via backpropagation and gradient descent. Then network is trained via generated input dataset of different sizes that is parameterized and passed the expected output for the trained inputs.
The network then predicts the output values and weights for each epoch is clearly shown to see the error.

*Steps followed:*
- Define number of inputs, hidden layers, outputs generically
- Define weights for each of the layers or connections using random number generator
- Get the sigmoid activation for each layer
- Get the sigmoid derivatives for each layer
- Iterate through each of the layers and compute the dot product of vectors between previous layer and associated weights defined
- Apply sigmoid function and store to be used for backpropagation
- For backpropagation, iterate backward through the network layers, get the stored activation from the previous layer and apply derivative function. 
- Get activation from the current layer, perform dot product of derivative and current activation
- Save derivative 

- Train the network with a number of epochs and learning rate
- Forward propagate
- Get error by differencing the actual output and expected output values
- Backpropagate the error
- Perform gradient descent and update the weights

-----------------------------------------------------------

**Results:**

Analyzed the neural network with different combinations of hidden layers and data sets.

For a dataset size of 100000 rows of data,

the network performed pretty good taking 4s for each epoch. And a total of 393s for a total of 1000 epochs

Tested For two inputs and two hidden layers with each of 5 nodes and 4 nodes and one output. 

Given dataset size is two columns with million dataset rows (1000000) and 20 epochs

- It took 46 seconds for running each of the epoch. For a total of 20 epochs the computation time observed is 16 minutes to train and evaluate the network. 

When the number of hidden layers is changed, the neural network behaved little different.

Given dataset size is two columns with 100000 dataset rows and 500 epochs

- It took 4 seconds for running each of the epoch. For a total of 500 epochs, the computation time has been greater than the averge time taken. Expected a 33.33 minutes to complete the training and evaluate. 
 But the network took a total of 48.4 minutes to complete.

When the number of hidden layers is changed, the neural network behaved little different.
Tested against three hidden layers with each of 5 4 3 nodes respectively. 

To analyze the breaking point, increased the dataset size to double 10000000 of a 100 epochs
At this data size, the program keeps running for almost 2 hours and system stopped responding not giving any results. Had to hard stop.

    
------------------------------------------------------------------------

**Reflection:**

Spending time on the theory and then building the feedforward neural network from scratch has led me understand the concepts more clearly. 
My initial take, was to utilize the python libraries like Sympy that allows to symbolically display and pretty print the equations in mathematical form and code from math standpoint.
But it is taking time to identify the library functions and apply them. So utilized numpy library and performed dot product for Matrix multiplication and iterating through the layers.
Analyzing the results has been time consuming, as it took hours to compute the results for larger data sizes. But was able to capture the results for different combinations.

